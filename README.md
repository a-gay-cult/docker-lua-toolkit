# docker-lua-toolkit

Image that contains tools that are useful for CI linting lua in our environment.

## Internal notes
Because of how much time it takes to build glualint, build this locally and push.
Don't use Gitlab CI for building if you value your sanity and build cache.

```sh
docker login registry.gitlab.com
docker build --force-rm --pull -t  registry.gitlab.com/a-gay-cult/docker-lua-toolkit/luatoolkit:latest .
docker push registry.gitlab.com/a-gay-cult/docker-lua-toolkit/luatoolkit:latest
```