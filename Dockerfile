# glualint build
FROM alpine:edge as glualint

RUN apk add --no-cache cabal ghc lua5.3 lua5.3-dev build-base git bash unzip ca-certificates openssl wget gmp-dev

WORKDIR /tmp

# Seperated into chunks because some portions take FORVER.
RUN git clone https://github.com/FPtje/GLuaFixer.git

WORKDIR /tmp/GLuaFixer
RUN cabal update
RUN cabal install happy
RUN cabal sandbox init
RUN cabal install --only-dependencies
RUN PATH=/tmp/GLuaFixer/.cabal-sandbox/bin:$PATH ./AGGenerator.sh
RUN cabal build glualint
RUN cp dist/build/glualint/glualint /

# rest of the image
FROM alpine:edge

RUN apk add --no-cache lua5.3 lua5.3-dev build-base git bash unzip ca-certificates openssl wget gmp libffi && \
    update-ca-certificates

# cabal path
RUN echo "export PATH=$HOME/.cabal/bin:$PATH" >> ~/.profile

# luarocks
RUN cd /tmp && \
    git clone https://github.com/luarocks/luarocks.git && \
    cd luarocks && \
    ./configure && \
    make build install && \
    cd .. && \
    rm -rf luarocks

# luacheck
RUN luarocks install luacheck

# glualint install
COPY --from=glualint /glualint /usr/bin/

WORKDIR /data
